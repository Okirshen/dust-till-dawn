use raylib::prelude::*;

// welcome to code hell

#[derive(Copy, Clone, PartialEq)]
enum Type {
    Empty,
    Sand,
    Water,
    Stone,
}

const CELL: i32 = 20;
const WIDTH: i32 = 50;
const HEIGHT: i32 = 25;
const TICK: f32 = 0.07;

fn main() {
    let (mut rl, thread) =  raylib::init()
        .size(WIDTH * (CELL + 1) + 1, HEIGHT * (CELL + 1) + 31)
        .title("Dust Till Dawn")
        .build();

    let mut grid: [[Type ; WIDTH as usize] ; HEIGHT as usize] = [[Type::Empty ; WIDTH as usize] ; HEIGHT as usize];
    grid[0][6] = Type::Sand;
    grid[5][6] = Type::Sand;
    grid[8][6] = Type::Sand;

    // rl.set_target_fps(1);

    let mut timer = 0.;
    let mut selected = Type::Stone;

    while !rl.window_should_close() {
        timer += rl.get_frame_time();

        let mouse = rl.get_mouse_position();
        let column = mouse.x as i32 / (CELL + 1);
        let row = mouse.y as i32 / (CELL + 1);
        if 0 <= column && column < WIDTH && 0 <= row  && row < HEIGHT {
            if rl.is_mouse_button_down(MouseButton::MOUSE_LEFT_BUTTON) && grid[row as usize][column as usize] == Type::Empty {
                grid[row as usize][column as usize] = selected;
            } else if rl.is_mouse_button_down(MouseButton::MOUSE_RIGHT_BUTTON) {
                grid[row as usize][column as usize] = Type::Empty;
            }
        }

        if rl.is_key_pressed(KeyboardKey::KEY_ONE) {
            selected = Type::Stone
        } else if rl.is_key_pressed(KeyboardKey::KEY_TWO) {
            selected = Type::Sand
        } else if rl.is_key_pressed(KeyboardKey::KEY_THREE) {
            selected = Type::Water
        }

        let mut d = rl.begin_drawing(&thread);

        d.clear_background(Color::BLACK);
        d.draw_text(format!("{} {}", column + 1, row+ 1).as_str(), 10, HEIGHT * (CELL + 1) + 10, 20, Color::WHITE);

        for x in 0..=WIDTH {
            d.draw_line(x * (CELL + 1) + 1, 0, x * (CELL + 1) + 1, (CELL + 1) * HEIGHT, Color::GRAY);
        }

        for y in 0..=HEIGHT {
            d.draw_line(0, y * (CELL + 1), (CELL + 1) * WIDTH , y * (CELL + 1) + 1, Color::GRAY);
        }

        let mut new_grid = grid.clone();
        for y in (0..HEIGHT).rev() {
            for x in (0..WIDTH).rev() {
                let cell = grid[y as usize][x as usize].clone();
                let color = match cell {
                    Type::Empty => Color::BLANK,
                    Type::Sand => {
                        let mut done = false;
                        if y < HEIGHT - 1 {
                            let new_cell = new_grid[y as usize + 1][x as usize];
                            if new_cell == Type::Water {
                                new_grid[y as usize][x as usize] = Type::Water;
                                new_grid[y as usize + 1][x as usize] = Type::Sand;
                                done = true;
                            } else if new_cell == Type::Empty {
                                new_grid[y as usize][x as usize] = Type::Empty;
                                new_grid[y as usize + 1][x as usize] = Type::Sand;
                                done = true;
                            }
                        }
                        if !done && y < HEIGHT - 1 && x > 0 {
                            let new_cell = new_grid[y as usize + 1][x as usize - 1];
                            if new_cell == Type::Water {
                                new_grid[y as usize][x as usize] = Type::Water;
                                new_grid[y as usize + 1][x as usize - 1] = Type::Sand;
                                done = true;
                            } else if new_cell == Type::Empty {
                                new_grid[y as usize][x as usize] = Type::Empty;
                                new_grid[y as usize + 1][x as usize - 1] = Type::Sand;
                                done = true;
                            }
                        }
                        if !done && y < HEIGHT - 1 && x < WIDTH - 1 {
                            let new_cell = new_grid[y as usize + 1][x as usize + 1];
                            if new_cell == Type::Water {
                                new_grid[y as usize][x as usize] = Type::Water;
                                new_grid[y as usize + 1][x as usize + 1] = Type::Sand;
                                // done = true;
                            } else if new_cell == Type::Empty {
                                new_grid[y as usize][x as usize] = Type::Empty;
                                new_grid[y as usize + 1][x as usize + 1] = Type::Sand;
                                // done = true;
                            }
                        }
                        Color::BEIGE
                    },
                    Type::Water => {
                        if y < HEIGHT - 1 && new_grid[y as usize + 1][x as usize] == Type::Empty {
                            new_grid[y as usize][x as usize] = Type::Empty;
                            new_grid[y as usize + 1][x as usize] = Type::Water;
                        } else if y < HEIGHT - 1 && x > 0 && new_grid[y as usize + 1][x as usize - 1] == Type::Empty {
                            new_grid[y as usize][x as usize] = Type::Empty;
                            new_grid[y as usize + 1][x as usize - 1] = Type::Water;
                        } else if y < HEIGHT - 1 && x < WIDTH - 1 && new_grid[y as usize + 1][x as usize + 1] == Type::Empty {
                            new_grid[y as usize][x as usize] = Type::Empty;
                            new_grid[y as usize + 1][x as usize + 1] = Type::Water;
                        } else if x > 0 && new_grid[y as usize][x as usize - 1] == Type::Empty {
                            new_grid[y as usize][x as usize] = Type::Empty;
                            new_grid[y as usize][x as usize - 1] = Type::Water;
                        } else if x < WIDTH - 1 && new_grid[y as usize][x as usize + 1] == Type::Empty {
                            new_grid[y as usize][x as usize] = Type::Empty;
                            new_grid[y as usize][x as usize + 1] = Type::Water;
                        }
                        Color::BLUE
                    },
                    Type::Stone => Color::GRAY,
                };

                d.draw_rectangle(x * (CELL + 1) + 1, y * (CELL + 1) + 1, CELL, CELL, color);
            }
        }
        
        if timer >= TICK {
            grid = new_grid;
            timer = 0.;
        }
    }
}
