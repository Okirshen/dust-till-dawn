use raylib::prelude::*;

#[derive(Copy, Clone)]
enum Type {
    Empty,
    Sand,
    Water,
}

struct GameSettings {
    cell_size: i32,
    length: i32,
    height: i32,
}

fn main() {
    let (mut rl, thread) =  raylib::init()
        .size(700, 700)
        .title("Dust Till Down")
        .build();

    let game = GameSettings {
        cell_size: 20,
        length: 10,
        height: 10,
    };

    let mut grid = [[Type::Empty; 640]; 480];
    grid[100][100] = Type::Sand;

    while !rl.window_should_close() {
        let mut d = rl.begin_drawing(&thread);
        
        for x in 0..(game.length + 1) {
            d.draw_line(x * (game.cell_size + 1), 0, x * (game.cell_size + 1), (game.cell_size + 1) * game.length, Color::WHITE);
        }


        for y in 0..game.height {
            for x in 0..game.length {
                let cell = grid[y as usize][x as usize].clone();
                let color = match cell {
                    Type::Empty => Color::BEIGE,
                    Type::Sand => Color::YELLOW,
                    Type::Water => Color::BLUE,
                };

                d.draw_rectangle(x * (game.cell_size + 1), y * (game.cell_size + 1), game.cell_size, game.cell_size, color);
                // d.draw_pixel(x as i32, y as i32, color);
                // println!("{} {} {}", x, y, grid[y as usize][x as usize]);
            }
        }

        d.clear_background(Color::RED);
    }
}
