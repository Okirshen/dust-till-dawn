# Dust Till Dawn

A very simple shadertoy clone

### Instalation
To install run:
```bash
cargo install dust_till_dawn
```

### Usage
Use `Left Mouse Click` to place a particle
Use `Right Mouse Click` to remove a particle
Use the Number Keys 1-3 to switch particle:
1. Stone
2. Sand
3. Water
